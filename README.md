Typescript Lambda Test
======================

Simple test project with typescript and lambda function.

## Setup

    yarn

## Development

This currently just watches the code using webpack but the plan is
to enable running in dev mode using the SAM cli and call your handler
with actual test events.

    yarn dev

## Production

    yarn --production
    env NODE_ENV=production yarn build
