import { Transform } from 'stream';

import log, { Logger } from '../utils/logger';

const LOG_PREFIX: string = '[JSONTRANSFORM]';

/**
 * Parses the input file data as JSON
 */
export class JSONTransform extends Transform {
  private log: Logger;
  private chunks: string[] = [];

  constructor() {
    super({
      objectMode: true,
    });
    this.log = log.getPrefixedLogger(LOG_PREFIX);
  }

  public _transform(chunk: string, encoding: string, callback: NodeCallback) {
    this.chunks.push(chunk);
    callback();
  }

  public _flush(callback: NodeCallback) {
    try {
      const data: string = this.chunks.join('');
      this.chunks.length = 0;
      const json: any[] = JSON.parse(data);
      if (this.log.isDebugEnabled()) {
        this.log.debug(`Decoded JSON with ${json && json.length} entries (${data.length} bytes)`);
      }
      json.forEach((feature: any) => this.push(feature));
      callback();
    } catch (err) {
      callback(err);
    }
  }
}
