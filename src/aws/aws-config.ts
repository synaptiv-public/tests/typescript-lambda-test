import AWS from 'aws-sdk';
import config from 'config';

import { AWSConfig } from '../models/Config';
import log from '../utils/logger';

const awsConfig: AWSConfig = config.get('aws');

export default new AWS.Config({
  ...awsConfig,
  logger: {
    log: (...args: any[]) => log.debug(...args),
  },
});

export function getConfig(options: any) {
  return new AWS.Config({
    ...awsConfig,
    logger: {
      log: (...args: any[]) => log.debug(...args),
    },
    ...options,
  });
}
