import AWS from 'aws-sdk';

import awsConfig, { getConfig } from './aws-config';

interface ClientCache {
  kinesis: AWS.Kinesis;
  s3: AWS.S3;
}

const clients: ClientCache = {
  kinesis: null,
  s3: null,
};

/**
 * Get a cached S3 client or create a new one
 */
export function getS3Client(): AWS.S3 {
  if (!clients.s3) {
    clients.s3 = new AWS.S3(awsConfig);
  }
  return clients.s3;
}

/**
 * Get a cached Kinesis client or create a new one
 */
export function getKinesisClient(): AWS.Kinesis {
  if (!clients.kinesis) {
    clients.kinesis = new AWS.Kinesis(getConfig({
      maxRetries: 10,
    }));
  }
  return clients.kinesis;
}
