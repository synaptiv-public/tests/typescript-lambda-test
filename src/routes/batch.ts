import { Express, Response } from 'express';

import { wrapErrors } from '../utils/express';
import logger from '../utils/logger';
import { ApiResponse, AuthorizedRequest } from './types';

interface BatchResponse {
  success: boolean;
}

const log = logger.getPrefixedLogger('[GET_DATA]');

/**
 * Invoke the normalize lambda function with the posted data
 * @param req
 * @param res
 */
async function getBatch(req: AuthorizedRequest, res: Response): Promise<void> {
  const response: ApiResponse<BatchResponse> = {
    data: {
      success: true,
    },
  };
  res.status(200).json(response);
}

export default (app: Express) => {
  app.get('/batch', wrapErrors(getBatch));
};
