// tslint:disable:max-func-args
import { NextFunction, Request, Response } from 'express';

import StatusCodeError from '../errors/StatusCodeError';
import logger from '../utils/logger';

const log = logger.getPrefixedLogger('[HANDLER]');

/**
 * Generic express error handler
 * @param err
 * @param req
 * @param res
 * @param next
 */
export function errorHandler(err: StatusCodeError, req: Request, res: Response, next: NextFunction) {
  const statusCode = err.statusCode || 500;
  if (statusCode >= 500) {
    log.error(err);
  }

  res.status(statusCode).json({
    error: err.message,
  });
}
