import { APIGatewayEvent } from 'aws-lambda';
import { Request } from 'express';

export type PromiseRouteHandler<TResult = any> = (request) => Promise<ApiResponse<TResult>>;

export interface User {
  username?: string;
  password?: string;
}

export interface ApiResponse<TData> {
  data?: TData | TData[];
  error?: string;
}

export interface GatewayRequest extends Request {
  apiGateway: {
    event: APIGatewayEvent;
  };
}

export interface AuthorizedRequest extends Request {
  user?: User;
}
