import { Express, Request, Response } from 'express';

import { ApiResponse } from './types';

interface Metrics {
  status: string;
}

function getMetrics(req: Request, res: Response): void {
  const response: ApiResponse<Metrics> = {
    data: {
      status: 'OK',
    },
  };
  res.json(response);
}

export default (app: Express) => {
  app.get('/metrics/health', getMetrics);
};
