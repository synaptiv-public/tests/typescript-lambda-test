export const ERROR_BAD_REQUEST: number = 400;
export const ERROR_UNAUTHORIZED: number = 401;
export const ERROR_NOT_FOUND: number = 404;
export const ERROR_SERVER: number = 500;

export default class StatusCodeError extends Error {
  public static create(code: number, msg: string): StatusCodeError {
    const err = new StatusCodeError(msg);
    err.statusCode = code;
    return err;
  }

  public statusCode?: number;
}
