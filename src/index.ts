import {
  APIGatewayProxyHandler,
  APIGatewayProxyResult,
} from 'aws-lambda';
import { createServer, proxy } from 'aws-serverless-express';
// tslint:disable-next-line:no-submodule-imports
import { eventContext } from 'aws-serverless-express/middleware';
import bodyParser from 'body-parser';
import config from 'config';
import cors from 'cors';
import express from 'express';

import registerBatchRoutes from './routes/batch';
import { errorHandler } from './routes/errors';
import registerMetricsRoutes from './routes/metrics';
import logger from './utils/logger';

const log = logger.getPrefixedLogger('[HANDLER]');

const app = express();
app.use(cors());
app.use(eventContext());
app.use(bodyParser.json({
  limit: config.get('endpoint.limit'),
}));
registerMetricsRoutes(app);
registerBatchRoutes(app);
app.use(errorHandler);

const server = createServer(app);

/**
 * Lambda handler entrypoint
 * @param event
 * @param context
 */
export const handler: APIGatewayProxyHandler = (event, context): Promise<APIGatewayProxyResult> => {
  if (log.isDebugEnabled()) {
    log.debug(`Received event:`, JSON.stringify(event));
  }
  return proxy(server, event, context, 'PROMISE').promise;
};
