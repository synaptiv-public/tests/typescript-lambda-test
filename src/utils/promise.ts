export type PromiseMapper = (value: any, key: number) => any | Promise<any>;

/**
 * Map over the collection but resolve promises first before moving to the next entry
 */
export function promiseMapSerial(collection: any[], mapper: PromiseMapper): Promise<any[]> {
  return collection.reduce(async (promise: Promise<any[]>, value: any, key: number): Promise<any[]> => {
    const results: any[] = await promise;
    const mapValue = await Promise.resolve(mapper(value, key));
    results.push(mapValue);

    return results;
  }, Promise.resolve([]));
}
