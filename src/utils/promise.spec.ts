import { promiseMapSerial } from './promise';

describe('Promise', () => {
  describe('.promiseMapSerial', () => {
    it('returns an array of mapped values if the mapper does not return a promise', async () => {
      const coll = [1, 2, 3];

      const mapped = await promiseMapSerial(coll, (val: number) => val + 10);

      expect(mapped).toEqual([11, 12, 13]);
    });

    it('waits for the previous promise to finish before continuing', async () => {
      const coll = [100, 2, 3];

      // this helps us track concurrency
      let isBusy = false;

      const mapped = await promiseMapSerial(coll, (val: number) => {
        expect(isBusy).toBe(false);

        isBusy = true;
        return new Promise(resolve => {
          setTimeout(() => {
            isBusy = false;
            resolve(val);
          }, val);
        });
      });

      expect(mapped).toEqual([100, 2, 3]);
    });
  });
});
