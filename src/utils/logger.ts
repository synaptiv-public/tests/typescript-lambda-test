// tslint:disable:no-console
import config from 'config';

export enum LogLevel {
  DEBUG = 'debug',
  ERROR = 'error',
  INFO = 'info',
}

export interface LogOptions {
  level: LogLevel;
  prefix?: string;
}

enum LevelMap {
  error = 1,
  warn = 2,
  debug = 3,
  trace = 4,
}

export class Logger {
  private level: number;
  private options: LogOptions;

  constructor(options: LogOptions) {
    this.options = options;
    this.level = LevelMap[options.level];
  }

  /**
   * Get a logger which prefixes all output
   * @param prefix
   */
  public getPrefixedLogger(prefix: string): Logger {
    return new Logger({
      ...this.options,
      prefix,
    });
  }

  public isDebugEnabled(): boolean {
    return this.level >= LevelMap.debug;
  }

  public isTraceEnabled(): boolean {
    return this.level >= LevelMap.trace;
  }

  public error = (...args: any[]): void => {
    if (this.level < LevelMap.error) return;
    console.error('[ERROR]', ...this.insertPrefix(args));
  };

  public warn = (...args: any[]): void => {
    if (this.level < LevelMap.warn) return;
    console.warn(...this.insertPrefix(args));
  };

  public debug = (...args: any[]): void => {
    if (!this.isDebugEnabled()) return;
    console.log(...this.insertPrefix(args));
  };

  public trace = (...args: any[]): void => {
    if (!this.isTraceEnabled()) return;
    console.log(...this.insertPrefix(args));
  };

  public info = (...args: any[]): void => {
    console.info(...this.insertPrefix(args));
  };

  private insertPrefix(args: any[]): any[] {
    const prefix: string = this.options.prefix;
    if (!prefix || !args || args.length === 0) return args;

    // make sure we keep support for the first arg to be a format string
    if (typeof args[0] === 'string') {
      return [`${prefix} ${args[0]}`, ...args.slice(1)];
    }

    return [prefix, ...args];
  }
}

export default new Logger(config.get('log'));
