import { NextFunction, Request, Response } from 'express';

import StatusCodeError, { ERROR_UNAUTHORIZED } from '../errors/StatusCodeError';
import { AuthorizedRequest, User } from '../routes/types';
import logger from './logger';

type RequestHandler = (req?: Request, res?: Response, next?: NextFunction) => void;
type RequestPromiseHandler = (req?: Request, res?: Response, next?: NextFunction) => Promise<any>;

const log = logger.getPrefixedLogger('[EXPRESS]');

export function wrapErrors(handler: RequestPromiseHandler): RequestHandler {
  return async (req?: Request, res?: Response, next?: NextFunction) => {
    try {
      await handler(req, res, next);
    } catch (err) {
      next(err);
    }
  };
}

export function parseBasicAuth(authorization: string): User {
  const [authType, authKey] = authorization.split(' ');

  if (authType.toLowerCase() !== 'basic' || !authKey) {
    return {};
  }

  const authString: string = (new Buffer(authKey, 'base64')).toString('utf8');
  const [username, password] = authString.split(':');

  return {
    password,
    username,
  };
}

export function checkAuthorization(req: AuthorizedRequest, res: Response, next: NextFunction) {
  if (!req.user || !req.user.username) {
    throw StatusCodeError.create(ERROR_UNAUTHORIZED, 'Unauthorized');
  }
  return next();
}
