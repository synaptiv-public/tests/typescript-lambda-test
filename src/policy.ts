import { CustomAuthorizerResult } from 'aws-lambda';

import logger from './utils/logger';

const log = logger.getPrefixedLogger('[POLICY]');

export function buildAllowAllPolicy(event, principalId): CustomAuthorizerResult {
  const tmp = event.methodArn.split(':');
  const apiGatewayArnTmp = tmp[5].split('/');
  const awsAccountId = tmp[4];
  const awsRegion = tmp[3];
  const restApiId = apiGatewayArnTmp[0];
  const stage = apiGatewayArnTmp[1];
  const apiArn = `arn:aws:execute-api:${awsRegion}:${awsAccountId}:${restApiId}/${stage}/*/*`;

  const policy = {
    policyDocument: {
      Statement: [
        {
          Action: 'execute-api:Invoke',
          Effect: 'Allow',
          Resource: [apiArn],
        },
      ],
      Version: '2012-10-17',
    },
    principalId,
  };
  if (log.isDebugEnabled()) log.debug('Policy:', JSON.stringify(policy));

  return policy;
}
