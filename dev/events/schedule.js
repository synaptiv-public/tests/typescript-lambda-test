let lambdaHandler;
let intervalHandle;

async function callHandler() {
  try {
    const time = (new Date()).toISOString();
    const event = {
      account: '123456789012',
      region: 'us-east-1',
      detail: {},
      'detail-type': 'Scheduled Event',
      source: 'aws.events',
      time,
      id: 'cdc73f9d-aea9-11e3-9d5a-835b769c0d9c',
      resources: [
        'arn:aws:events:us-east-1:123456789012:rule/my-schedule',
      ]
    };
    await lambdaHandler(event);
  } catch (err) {
    console.error('Error invoking handler:', err);
  }
}

export function start(handler) {
  lambdaHandler = handler;
  intervalHandle = setInterval(callHandler, 60000);
  return callHandler();
}

export function pause() {
  lambdaHandler = null;
  if (intervalHandle) {
    clearInterval(intervalHandle);
  }
}

export function stop() {
  lambdaHandler = null;
  if (intervalHandle) {
    clearInterval(intervalHandle);
  }
}
